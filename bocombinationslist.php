<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once _PS_MODULE_DIR_ . 'bocombinationslist/classes/CLConfig.php';
include_once _PS_MODULE_DIR_ . 'bocombinationslist/classes/BCL.php';

class Bocombinationslist extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'bocombinationslist';
        $this->tab = 'administration';
        $this->version = '1.3.1';
        $this->author = 'Ciren';
        $this->need_instance = 1;
        $this->module_key = 'c152dc26f49c9dee40f4cbd49b08430a';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('List of combinations');
        $this->description = $this->l('Display a list of all combinations with custom columns');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

        $this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => '8.0.99');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
            $position = 1;
        } else {
            $position = 0;
        }

        Db::getInstance()->execute("TRUNCATE TABLE `" . _DB_PREFIX_ . "bocombinationslist`");

        $sql = array();

        $sql[] = array(
            'position' => $position,
            'attribute' => 'id_product',
            'table' => 'product',
            'title' => 'ID ' . $this->l('Product'),
            'type' => 'integer'
        );

        $sql[] = array(
            'position' => $position + 1,
            'attribute' => 'id_product_attribute',
            'table' => 'product_attribute',
            'title' => 'ID ' . $this->l('Combination'),
            'type' => 'integer'
        );

        $sql[] = array(
            'position' => $position + 2,
            'attribute' => 'id_image',
            'table' => 'product_attribute_image',
            'title' => $this->l('Image'),
            'type' => ''
        );

        $sql[] = array(
            'position' => $position + 3,
            'attribute' => 'name',
            'table' => 'product_lang',
            'title' => $this->l('Product'),
            'type' => ''
        );

        $sql[] = array(
            'position' => $position + 4,
            'attribute' => 'name',
            'table' => 'attribute_lang',
            'title' => $this->l('Attributes'),
            'type' => ''
        );

        $sql[] = array(
            'position' => $position + 5,
            'attribute' => 'price',
            'table' => 'bocombinationslist',
            'title' => $this->l('Price (Tax Excl.)'),
            'type' => 'price'
        );

        $sql[] = array(
            'position' => $position + 6,
            'attribute' => 'final_price',
            'table' => 'bocombinationslist',
            'title' => $this->l('Price (Tax Incl.)'),
            'type' => 'price'
        );

        $sql[] = array(
            'position' => $position + 7,
            'attribute' => 'quantity',
            'table' => 'stock_available',
            'title' => $this->l('Quantity'),
            'type' => 'integer'
        );

        foreach ($sql as $query) {
            Db::getInstance()->insert('bocombinationslist', $query);
        }

        return parent::install() &&
            $this->addTab() &&
            $this->registerHook('actionAdminCombinationsListListingResultsModifier') &&
            $this->registerHook('displayBackOfficeHeader');
    }

    private function addTab()
    {
        $tab = new Tab();
        $tab->class_name = 'AdminCombinationsList';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminCatalog');
        $tab->module = $this->name;
        $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Combinations');
        $tab->add();

        if (Tab::getIdFromClassName('AdminLists') === false) {
            $tab = new Tab();
            $tab->class_name = 'AdminLists';
            $tab->icon = 'view_list';
            $tab->id_parent = (int)Tab::getIdFromClassName('IMPROVE');
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Lists');
            $tab->add();
        }

        $tab = new Tab();
        $tab->class_name = 'AdminCombinationsListConfig';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminLists');
        $tab->module = $this->name;
        $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Customize combinations list columns');
        $tab->add();

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        include(dirname(__FILE__).'/sql/uninstall.php');

        return $this->removeTab();
    }

    private function removeTab()
    {
        $ids_tab = array(
            (int)Tab::getIdFromClassName('AdminCombinationsList'),
            (int)Tab::getIdFromClassName('AdminCombinationsListConfig'),
        );
        foreach ($ids_tab as $id_tab) {
            if ($id_tab) {
                $tab = new Tab($id_tab);
                $tab->delete();
            }
        }

        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $link = new Link();
        $link = $link->getAdminLink('AdminCombinationsListConfig', true);
        Tools::redirectAdmin($link);
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookDisplayBackOfficeHeader()
    {
        if (!Module::isEnabled($this->name)) {
            return false;
        }

        $js_var = '<script>
        var admin_combinationslist_ajax_url = ' . (string) json_encode(
            $this->context->link->getAdminLink('AdminCombinationsListConfig')
        ) . ';var current_id_tab = ' . (int) $this->context->controller->id . ';
        </script>';

        switch (strtolower(Tools::getValue('controller'))) {
            case 'admincombinationslistconfig':
                return $js_var;
            case 'admincombinationslist':
                if (version_compare(_PS_VERSION_, '1.6.0.12', '<')) {
                    $this->context->smarty->assign(array(
                        'id_form' => '#product_attribute'
                    ));
                } else {
                    $this->context->smarty->assign(array(
                        'id_form' => '#form-product_attribute'
                    ));
                }

                return $js_var . $this->context->smarty->fetch($this->local_path.'views/templates/admin/editable.tpl');
        }

        return false;
    }

    public function hookActionAdminCombinationsListListingResultsModifier($params)
    {
        if (!array_key_exists('0', $params['list'])) {
            return;
        }

        if (!array_key_exists('bocombinationslistfinal_price', $params['list'][0]) &&
            !array_key_exists('bocombinationslistprice', $params['list'][0])) {
            return;
        }

        foreach ($params['list'] as &$value) {
            $value['bocombinationslistfinal_price'] = Product::getPriceStatic(
                (int)$value['id_product'],
                true,
                (int)$value['id_product_attribute'],
                6,
                null,
                false,
                false
            );

            $value['bocombinationslistprice'] = Product::getPriceStatic(
                (int)$value['id_product'],
                false,
                (int)$value['id_product_attribute'],
                6,
                null,
                false,
                false
            );
        }
    }
}
