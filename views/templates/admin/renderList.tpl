{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
$('document').ready(function() {
  let table = '{$id_table|escape:'htmlall':'UTF-8'}';
  let form = '{$id_form|escape:'htmlall':'UTF-8'}';
  $(table + ' td.pointer').each(function() {
    $(this).removeAttr('onclick');
    $(this).removeClass('pointer');
  });
  $(table + ' td.cl-titles').each(function() {
    $(this)[0].innerHTML = "<input type='text' class='cl-input-text' value=\"" + $(this)[0].innerText + "\">"
  });

  $(form).on('change', '.cl-input-text', function(){
    $.ajax({
      type: 'POST',
      url: admin_combinationslist_ajax_url,
      dataType: 'json',
      data: {
        controller : 'AdminCombinationsListConfig',
        action : 'sendInputList',
        ajax : true,
        attribute: 'title',
        BCL_input : $(this).val(),
        id_bocombinationslist : $(this).parent().parent().attr('id').split('_')[2],
      },
      {if (version_compare(_PS_VERSION_, '1.7.0.0', '>='))}
      success: function() {
        $.growl.notice({ title: "", message: "{l s='Successful update' mod='bocombinationslist'}" });
      },
      error: function() {
        $.growl.error({ title: "", message: "{l s='An error has occurred' mod='bocombinationslist'}" });
      }
      {/if}
    });
  });
  $(form).on('change', '.cl-select-type', function(){
    $.ajax({
      type: 'POST',
      url: admin_combinationslist_ajax_url,
      dataType: 'json',
      data: {
        controller : 'AdminCombinationsListConfig',
        action : 'sendInputList',
        ajax : true,
        attribute: 'type',
        BCL_input : $(this).val(),
        id_bocombinationslist : $(this).parent().parent().attr('id').split('_')[2],
      },
      {if (version_compare(_PS_VERSION_, '1.7.0.0', '>='))}
      success: function() {
        $.growl.notice({ title: "", message: "{l s='Successful update' mod='bocombinationslist'}" });
      },
      error: function() {
        $.growl.error({ title: "", message: "{l s='An error has occurred' mod='bocombinationslist'}" });
      }
      {/if}
    });
  });
});
</script>
