{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
	attributesUpdate();
	var html;


	$(".available").click(function() {
		let position = $("#attributes_position div:last-child .position")[0] ? parseInt($("#attributes_position div:last-child .position")[0].innerText) + 1 : 1;
		select = "<select id=\"BCL_TYPE_"+position+"\" name=\"BCL_TYPE_"+position+"\" class=\"attribute_type\"><option value=\"\" >{l s='Type' mod='bocombinationslist'}</option>";
		{foreach from=$types item=type key=value}
		if ($(this).val() == '{$value}') {
			select +=	"<option value=\"{$value|escape:'htmlall':'UTF-8'}\" selected=\"selected\">{$type|escape:'htmlall':'UTF-8'}</option>";
		} else {
			select +=	"<option value=\"{$value|escape:'htmlall':'UTF-8'}\">{$type|escape:'htmlall':'UTF-8'}</option>";
		}
		{/foreach}
		select +=	"</select>";

		html = "<div class=\"panel col-lg-4\">";
		html += "<table class=\"table\">";
		html += "<button type=\"button\" class=\"close\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
		html += "<tr>";
		html += "<th class=\"attribute_id\">";
		html += $(this).data('field');
		html += "</th>";
		html += "</tr>";
		html += "<tr>";
		html += "<td>";
		html += "<input class=\"title\" type=\"text\" name=\"BCL_TITLE_"+position+"\">";
		html += "</td>";
		html += "</tr>";
		html += "<tr>";
		html += "<td>";
		html += select;
		html += "</td>";
		html += "</tr>";
		html += "<tr>";
		html += "<td class=\"attribute_name\">";
		html += $(this).data('table');
		html += "</td>";
		html += "</tr>";
		html += "<tr>";
		html += "<td class=\"attribute_position\">";
		html += "Position : <span class=\"position\">"+position+"</span>";
		html += " <button type=\"button\" class=\"btn btn-default plus\">+</button> <button type=\"button\" class=\"btn btn-default moins\">-</button>";
		html += "</td>";
		html += "</tr>";
		html += "</table>";
		html += "<input class=\"aHidden\" type=\"hidden\" name=\"BCL_ATTRIBUTE_"+position+"\" value=\""+$(this).data('field')+"\">";
		html += "<input class=\"tHidden\" type=\"hidden\" name=\"BCL_TABLE_"+position+"\" value=\""+$(this).data('table')+"\">";
		html += "</div>";

		$('#attributes_position').append(html);
		attributesUpdate();
		$.growl.notice({ title: "", message: "{l s='Successful add' mod='bocombinationslist'}" });
	});

	$("#attributes_position").on('click', 'button.plus', function() {
		let position = parseInt($(this)[0].parentElement.firstElementChild.innerText);
		if(position < parseInt($("#attributes_position div:last-child .position")[0].innerText)) {
			let id_to_up = $("#attributes_position div:nth-child("+parseInt(position)+") .attribute_id")[0].innerText;
			let name_to_up = $("#attributes_position div:nth-child("+parseInt(position)+") .attribute_name")[0].innerText;
			let text_to_up = $("#attributes_position div:nth-child("+parseInt(position)+") .title")[0].value;
			let type_to_up = $("#attributes_position div:nth-child("+parseInt(position)+") .type")[0].value;
			let id_to_down = $("#attributes_position div:nth-child("+parseInt(position+1)+") .attribute_id")[0].innerText;
			let name_to_down = $("#attributes_position div:nth-child("+parseInt(position+1)+") .attribute_name")[0].innerText;
			let text_to_down = $("#attributes_position div:nth-child("+parseInt(position+1)+") .title")[0].value;
			let type_to_down = $("#attributes_position div:nth-child("+parseInt(position+1)+") .type")[0].value;

			$("#attributes_position div:nth-child("+parseInt(position)+") .aHidden")[0].value = id_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .attribute_id")[0].innerText = id_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .tHidden")[0].value = name_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .attribute_name")[0].innerText = name_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .title")[0].value = text_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .type")[0].value = type_to_down;
			$("#attributes_position div:nth-child("+parseInt(position+1)+") .aHidden")[0].value = id_to_up;
			$("#attributes_position div:nth-child("+parseInt(position+1)+") .attribute_id")[0].innerText = id_to_up;
			$("#attributes_position div:nth-child("+parseInt(position+1)+") .tHidden")[0].value = name_to_up;
			$("#attributes_position div:nth-child("+parseInt(position+1)+") .attribute_name")[0].innerText = name_to_up;
			$("#attributes_position div:nth-child("+parseInt(position+1)+") .title")[0].value = text_to_up;
			$("#attributes_position div:nth-child("+parseInt(position+1)+") .type")[0].value = type_to_up;

			attributesUpdate();
		}
	});

	$("#attributes_position").on('click', 'button.moins', function() {
		let position = parseInt($(this)[0].parentElement.firstElementChild.innerText);
		if(position > 1 ) {
			let id_to_up = $("#attributes_position div:nth-child("+parseInt(position)+") .attribute_id")[0].innerText;
			let name_to_up = $("#attributes_position div:nth-child("+parseInt(position)+") .attribute_name")[0].innerText;
			let text_to_up = $("#attributes_position div:nth-child("+parseInt(position)+") .title")[0].value;
			let type_to_up = $("#attributes_position div:nth-child("+parseInt(position)+") .type")[0].value;
			let id_to_down = $("#attributes_position div:nth-child("+parseInt(position-1)+") .attribute_id")[0].innerText;
			let name_to_down = $("#attributes_position div:nth-child("+parseInt(position-1)+") .attribute_name")[0].innerText;
			let text_to_down = $("#attributes_position div:nth-child("+parseInt(position-1)+") .title")[0].value;
			let type_to_down = $("#attributes_position div:nth-child("+parseInt(position-1)+") .type")[0].value;

			$("#attributes_position div:nth-child("+parseInt(position)+") .aHidden")[0].value = id_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .attribute_id")[0].innerText = id_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .tHidden")[0].value = name_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .attribute_name")[0].innerText = name_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .title")[0].value = text_to_down;
			$("#attributes_position div:nth-child("+parseInt(position)+") .type")[0].value = type_to_down;
			$("#attributes_position div:nth-child("+parseInt(position-1)+") .aHidden")[0].value = id_to_up;
			$("#attributes_position div:nth-child("+parseInt(position-1)+") .attribute_id")[0].innerText = id_to_up;
			$("#attributes_position div:nth-child("+parseInt(position-1)+") .tHidden")[0].value = name_to_up;
			$("#attributes_position div:nth-child("+parseInt(position-1)+") .attribute_name")[0].innerText = name_to_up;
			$("#attributes_position div:nth-child("+parseInt(position-1)+") .title")[0].value = text_to_up;
			$("#attributes_position div:nth-child("+parseInt(position-1)+") .type")[0].value = type_to_up;

			attributesUpdate();
		}
	});

	$("#attributes_position").on('click', 'button.close', function() {
		let position = parseInt($(this)[0].nextSibling.firstElementChild.lastElementChild.firstElementChild.firstElementChild.innerText);
		let newPosition;

		$("#attributes_position div").each(function() {
			if (parseInt($(this)[0].children[1].firstElementChild.lastElementChild.firstElementChild.firstElementChild.innerText) > position) {
				$(this)[0].children[1].firstElementChild.lastElementChild.firstElementChild.firstElementChild.innerText -= 1;
				newPosition = $(this)[0].children[1].firstElementChild.lastElementChild.firstElementChild.firstElementChild.innerText;
				$(this)[0].children[2].name = "BCL_ATTRIBUTE_" + newPosition;
				$(this)[0].children[3].name = "BCL_TABLE_" + newPosition;
				$(this)[0].children[1].firstElementChild.children[1].firstElementChild.firstElementChild.name = "BCL_TITLE_" + newPosition;
				$(this)[0].children[1].firstElementChild.children[2].firstElementChild.firstElementChild.name = "BCL_TYPE_" + newPosition;
			}
		});

		$(this)[0].parentElement.remove();
		attributesUpdate();

	})

	$("#attributes_position").on('change', 'input.title', function() {
		attributesUpdate();
	});

	$("#attributes_position").on('change', 'select.type', function() {
		attributesUpdate();
	});

	function attributesUpdate() {
		let attributes = [];
		let tables = [];
		let titles = [];
		let types = [];
		$("#attributes_position div").each(function() {
			attributes.push($(this)[0].children[2].value);
			tables.push($(this)[0].children[3].value);
			titles.push($(this)[0].children[1].firstElementChild.children[1].firstElementChild.firstElementChild.value);
			types.push($(this)[0].children[1].firstElementChild.children[2].firstElementChild.firstElementChild.value);
		});
		$("#globalAtt").val(attributes.join(';'));
		$("#globalTab").val(tables.join(';'));
		$("#globalTit").val(titles.join(';'));
		$("#globalTyp").val(types.join(';'));
	}
</script>
