{*
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="row">
	<div class="col-lg-12">
    <form class="defaultForm form-horizontal" name="BCL_GLOBAL" method="POST" >
      <div class="panel container">
        <div class="panel-heading"><i class="icon-plus"></i> {l s='Quick add' mod='bocombinationslist'}</div>
        <div class="panel-body">
				<label for="bocombinationslist_quick_add" class="control-label col-lg-5">
					{l s='Quick add' mod='bocombinationslist'}:
				</label>
				<div class="col-lg-7">
					<select name="BCL_QUICK_ADD" class="fixed-width-xl col-lg-2" id="bocombinationslist_quick_add">
						{foreach from=$quick_add item=category_array key=category_name}
							<optgroup label="{$category_name|escape:'htmlall':'UTF-8'}">
							{foreach from=$category_array item=column_key}
								<option value="{$column_key|escape:'htmlall':'UTF-8'}">{$description[$column_key]['description']|escape:'htmlall':'UTF-8'}</option>
							{/foreach}
						{/foreach}
					</select><br><br>
					<p class="help-block col-lg-7">{l s='If you don\'t find the attribute you are looking for, please click on the button "Add a new column" at the top right of the page' mod='bocombinationslist'}</p>
				</div>
				</div>
        <div class="panel-footer">
          <button type="submit" value="1" name="submitQuickAdd" class="btn btn-default pull-right">
            <i class="process-icon-save"></i>
            {l s='Add' mod='bocombinationslist'}
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
