<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once _PS_MODULE_DIR_ . 'bocombinationslist/classes/CLConfig.php';
include_once _PS_MODULE_DIR_ . 'bocombinationslist/classes/BCL.php';

class AdminCombinationsListController extends ModuleAdminController
{
    protected $bcl;
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'product_attribute';
        $this->className = 'Product';
        $this->tabClassName = 'AdminCombinationsList';
        $this->lang = false;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array(
        'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        parent::__construct();

//        if (Tools::getIsset('id_category') && !Tools::getValue('ajax')) {
//            if (Category::categoryExists((int)Tools::getValue('id_category'))) {
//                $this->context->cookie->__set('bcl_id_category', (int)Tools::getValue('id_category'));
//            } else {
//                $this->context->cookie->__unset('bcl_id_category');
//                $this->context->cookie->__unset('combinationslistproductOrderby');
//            }
//        }

        $this->addRowAction('View');

        $this->bcl = new BCL();
        $this->bcl->setUseParent(true);
    }

    public function init()
    {
        if (!$this->ajax) {
            // Do not move, otherwise the filters will not work
            $this->bcl->setDefaultJoin()
                ->setQuery();

            if (empty($this->_select)) {
                $this->_select = "";
            }
            if (empty($this->_join)) {
                $this->_join = "";
            }
            if (empty($this->_where)) {
                $this->_where = "";
            }

            $this->_select .= implode(', ', $this->bcl->getSelect());
            $this->_join .= implode(' ', $this->bcl->getJoin());
            $this->_where .= $this->bcl->getDefaultWhere();
            $this->_group = $this->bcl->getDefaultGroupBy();
            $this->_orderBy = $this->bcl->getDefaultOrderBy();
            $this->_orderWay = $this->bcl->getDefaultOrderWay();

            $this->fields_list = $this->bcl->getFields();
        }

        parent::init();
    }

    public function renderForm()
    {
        $link = new Link();
        $link = $link->getAdminLink('AdminProducts', false);

        Tools::redirectAdmin($link);
    }

    public function renderList()
    {
        return parent::renderList();
    }

    public function initToolbar()
    {
        parent::initToolbar();

        $link = "index.php/sell/catalog/products/new";

        $id_employee = (int)$this->context->employee->id;

        $url = Context::getContext()->link->getBaseLink() . basename(_PS_ADMIN_DIR_) . '/';

        $this->toolbar_btn['new']['href'] = $url . $link . '?token=' . Tools::getAdminToken($id_employee);
    }

    public function renderView()
    {
        $id_product_attribute = (int)Tools::getValue('id_product_attribute');
        $id_product = (int)Db::getInstance()->getValue(
            "SELECT `id_product` 
            FROM `" . _DB_PREFIX_ . "product_attribute` 
            WHERE `id_product_attribute` = " . (int)$id_product_attribute
        );

        $link = new Link();
        if (version_compare(_PS_VERSION_, '1.7.1.0', '>=')) {
            $link = $link->getAdminLink('AdminProducts', true, ['id_product' => $id_product]);
        } else {
            $link = $link->getAdminLink('AdminProducts', true) .
                "&id_product=" . $id_product . "&updateproduct";
        }

        Tools::redirectAdmin($link);
    }

    public function printCustomBoolean0($value, $tr)
    {
        return $this->printCustomBoolean($value, $tr, 0);
    }

    public function printCustomBoolean1($value, $tr)
    {
        return $this->printCustomBoolean($value, $tr, 1);
    }

    public function printCustomBoolean2($value, $tr)
    {
        return $this->printCustomBoolean($value, $tr, 2);
    }

    public function printCustomBoolean3($value, $tr)
    {
        return $this->printCustomBoolean($value, $tr, 3);
    }

    public function printCustomBoolean4($value, $tr)
    {
        return $this->printCustomBoolean($value, $tr, 4);
    }

    public function printCustomBoolean5($value, $tr)
    {
        return $this->printCustomBoolean($value, $tr, 5);
    }

    public function printCustomBoolean6($value, $tr)
    {
        return $this->printCustomBoolean($value, $tr, 6);
    }

    public function printCustomBoolean7($value, $tr)
    {
        return $this->printCustomBoolean($value, $tr, 7);
    }

    public function printCustomBoolean($value, $tr, $n_boolean)
    {
        $fields = BCL::getBooleanColumns();
        if (array_key_exists($n_boolean, $fields)) {
            $field = $fields[$n_boolean];
        } else {
            return ($value ? $this->l('Yes') : $this->l('No'));
        }
        return "<a class='list-action-enable " . ($value ? 'action-enabled' : 'action-disabled') . "'" .
            "href='index.php?tab=" . $this->tabClassName .
            "&action=custom_boolean" .
            "&token=" . Tools::getAdminTokenLite($this->tabClassName) .
            "&id_product=" . (int)$tr['id_product'] .
            "&id_combination=" . (int)$tr['id_product_attribute'] .
            "&table=" . $field['table'] .
            "&attribute=" . $field['attribute'] . "'>
            " . ($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>') . "
             </a>";
    }

    public function processCustomBoolean()
    {
        $update = $this->updateValue();
        if (!$update['success']) {
            $this->errors[] = $update['text'];
            return false;
        }

        Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . pSQL($update['table']) . "`
            SET `" . pSQL($update['attribute']) . "` = '" . ($update['attribute_value'] ? '0' : '1') . "'
            WHERE 1" . $update['where_update']
        );

        if ($update['table'] == 'product_attribute') {
            $is_shop = (bool)Db::getInstance()->getValue(
                "SELECT `column_name`
                FROM `information_schema`.`columns`
                WHERE `table_schema` = '" . _DB_NAME_ . "'
                AND `table_name` = '" . _DB_PREFIX_ . "product_attribute_shop'
                AND `column_name` = '" . $update['attribute'] . "'"
            );

            if ($is_shop) {
                Db::getInstance()->execute(
                    "UPDATE " . _DB_PREFIX_ . "product_attribute_shop
                    SET `" . pSQL($update['attribute']) . "` = '" . ($update['attribute_value'] ? '0' : '1') . "'
                    WHERE `id_product` = '" . (int)$update['id_product'] . "'
                    AND `id_product_attribute` = '" . (int)$update['id_combination'] . "'" .
                    Shop::addSqlRestriction(false, false)
                );
            }
        }

        $link = $this->context->link->getAdminLink('AdminCombinationsList', true);
        Tools::redirectAdmin($link . '&conf=4');
    }

    public function updateValue()
    {
        $table = pSQL(Tools::getValue('table'));
        $attribute = PSQL(Tools::getValue('attribute'));
        $id_product = (int)Tools::getValue('id_product');
        $id_combination = (int)Tools::getValue('id_combination');

        $exists = Db::getInstance()->getValue(
            "SELECT `column_name`
            FROM `information_schema`.`COLUMNS`
            WHERE `table_schema` = '" . _DB_NAME_ . "'
            AND `table_name` = '" . _DB_PREFIX_ . pSQL($table) . "'
            AND `column_name` = '" . pSQL($attribute) . "'"
        );

        if (!$exists) {
            return array('success' => false, 'text' => $this->l('An error has occurred.'));
        }

        // DO NOT USE $this->>bcl
        $bcl = new BCL(false);
        $where = $bcl->getDefaultWhere();
        $bcl->setDefaultJoin()->addDefaultJoin($table);

        if (!in_array($table, $bcl->default_tables)) {
            // JOIN
            $columns = Db::getInstance()->executeS(
                "SHOW COLUMNS FROM `" . _DB_PREFIX_ . pSQL($table) . "`"
            );
            $return = true;

            $base_on = '';
            $join_on = '';
            $join_table = '';
            $lang = false;

            foreach ($bcl->fields as $join_table_temp => $field) {
                if (is_array($field)) {
                    $join_table_temp = $field['table'];
                } else {
                    $field = array(
                        'base_on' => $field,
                        'join_on' => $field,
                    );
                }

                foreach ($columns as $column) {
                    if ($column['Field'] == $field['join_on'] && $join_on == '') {
                        $base_on = $field['base_on'];
                        $join_on = $field['join_on'];
                        $join_table = $join_table_temp;
                        $bcl->addDefaultJoin($join_table);
                        $return = false;
                    } elseif ($column['Field'] == 'id_lang') {
                        $lang = true;
                    }

                    if ($lang && $join_on != '') {
                        break 2;
                    }
                }
            }

            if ($return) {
                return array('success' => false, 'text' => $this->l('An error has occurred.'));
            }

            $on = '`' . pSQL($table) . '`.`' . pSQL($join_on) . '` =
                    `' . pSQL($join_table) . '`.`' . pSQL($base_on) . '`';
            if ($lang && preg_match('/.*_lang$/', $table)) {
                $on .= " AND `" . pSQL($table) . "`.`id_lang` = '" . (int)$this->context->language->id . "'";
            }

            $bcl->joinQuery(
                'left',
                pSQL($join_table),
                pSQL($table),
                pSQL($table),
                $on
            );
        }

        try {
            $row = Db::getInstance()->getRow(
                "SELECT `" . pSQL($table) . "`.*
                FROM `" . _DB_PREFIX_ . "product_attribute` `product_attribute` " . /*
                FROM `" . _DB_PREFIX_ . "product` `product`
                LEFT JOIN `" . _DB_PREFIX_ . "product_lang` `product_lang`
                ON `product`.`id_product` = `product_lang`.`id_product`
                AND `product_lang`.`id_lang` = '" . (int)$this->context->language->id . "' " . */
                implode(' ', $bcl->getJoin()) . " WHERE 1 " . $where .
                (empty($id_product) ? " " : " AND `product`.`id_product` = '" . (int)$id_product . "' ") .
                (
                    empty($id_combination) ?
                    " " :
                    " AND `product_attribute`.`id_product_attribute` = '" . (int)$id_combination . "' "
                ) . "GROUP BY `product`.`id_product`
                ORDER BY `product`.`id_product` DESC"
            );
        } catch (\Exception $e) {
            return array('success' => false, 'text' => $e->getMessage());
        }
        if (empty($row)) {
            return array('success' => false, 'text' => $this->l('Unable to change value.'));
        }
        $where_update = "";
        foreach ($row as $key => $column) {
            if ($key != $attribute && !empty($column)) {
                $where_update .= " AND `" . pSQL($key) . "` = '" . pSQL($column, true) . "'";
            }
        }

        return array(
            'success' => true,
            'table' => $table,
            'attribute' => $attribute,
            'attribute_value' => $row[$attribute],
            'where_update' => $where_update,
            'id_product' => (isset($row['id_product']) ? (int)$row['id_product'] : 0),
            'id_combination' => (isset($row['id_product_attribute']) ? (int)$row['id_product_attribute'] : 0)
        );
    }
}
