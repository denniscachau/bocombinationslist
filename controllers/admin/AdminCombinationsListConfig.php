<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminCombinationsListConfigController extends ModuleAdminController
{
    protected $position_identifier = 'id_bocombinationslist';
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'bocombinationslist';
        $this->identifier = 'id_bocombinationslist';
        $this->className = 'CLConfig';
        $this->tabClassName = 'AdminCombinationsListConfig';
        $this->lang = false;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array(
          'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        $this->_defaultOrderBy = 'position';

        parent::__construct();

        $this->fields_list = array(
            'position' => array(
                'title' => $this->l('Position'),
                'align' => 'text-center',
                'position' => 'position',
                'filter_key' => 'position',
                'class' => 'fixed-width-xs',
                'search' => false,
                'orderby' => false
            ),
            'attribute' => array(
                'title' => $this->l('Attributes'),
                'align' => 'text-center',
                'filter_key' => 'attribute',
                'search' => false,
                'orderby' => false
            ),
            'table' => array(
                'title' => $this->l('Table'),
                'align' => 'text-center',
                'filter_key' => 'table',
                'search' => false,
                'orderby' => false
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'align' => 'text-center',
                'class' => 'cl-titles',
                'search' => false,
                'orderby' => false
            ),
            'type' => array(
                'title' => $this->l('Type'),
                'align' => 'text-center',
                'class' => 'cl-types',
                'callback' => 'printTypeSelect',
                'callback_object' => $this,
                'search' => false,
                'orderby' => false
            ),
        );

        $this->types = array(
            'boolean' => $this->l('Boolean (True / False)'),
            'price' => $this->l('Price'),
            'integer' => $this->l('Integer'),
            'decimal' => $this->l('Decimal'),
            'percent' => $this->l('Percentage'),
            'date' => $this->l('Date'),
            'datetime' => $this->l('Date / time'),
            'editable' => $this->l('Editable'),
        );

        $this->quick_add = array(
            $this->l('Default') => array(
                'product-id_product',
                'product_attribute-id_product_attribute',
                'product_attribute_image-id_image',
                'product_lang-name',
                'attribute_lang-name',
                'product_attribute-reference',
                'bocombinationslist-price',
                'bocombinationslist-final_price',
                'stock_available-quantity',
            ),
            $this->l('Combination') => array(
                'bocombinationslist-weight',
                'product_attribute-ean13',
                'product_attribute-upc',
                'product_attribute-location',
            ),
            $this->l('Product') => array(
                'product-reference',
                'category_lang-name',
                'product-ean13',
                'product-upc',
                'product-width',
                'product-height',
                'product-depth',
                'product-weight',
                'tag-name',
                'manufacturer-name',
                'feature_value_lang-value',
                'product-active'
            ),
        );
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            array_unshift($this->quick_add[$this->l('Product')], 'product-isbn');
            array_unshift($this->quick_add[$this->l('Combination')], 'product_attribute-isbn');
        }
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['desc-module-back'] = array(
                'href' => 'index.php?controller=AdminModules&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back'),
                'icon' => 'process-icon-back',
            );
            $this->page_header_toolbar_btn['desc-module-new'] = array(
                'href' => 'index.php?controller=' . $this->tabClassName . '&add' . $this->table . '&token=' .
                Tools::getAdminTokenLite($this->tabClassName),
                'desc' => $this->l('Add a new column'),
                'icon' => 'process-icon-new',
            );
            $this->page_header_toolbar_btn['desc-module-doc'] = array(
                'href' => _MODULE_DIR_ . 'bocombinationslist/readme_' . $this->getDocSuffix() . '.pdf',
                'desc' => $this->l('Read the documentation'),
                'target' => true,
                'icon' => 'process-icon- icon-file-text',
            );
        }
    }

    private function getDocSuffix()
    {
        $available_docs = array(
            'fr'
        );

        return in_array($this->context->language->iso_code, $available_docs) ?
            $this->context->language->iso_code :
            'en';
    }

    private function assignation()
    {
        $columns_name = array(
            'id_product'
        );

        $tables_name = array(
            _DB_PREFIX_ . 'tag',
            _DB_PREFIX_ . 'manufacturer',
            _DB_PREFIX_ . 'attribute_lang',
            _DB_PREFIX_ . 'supplier',
            _DB_PREFIX_ . 'feature_value_lang',
        );

        $non_tables = array(
            _DB_PREFIX_ . 'product_tag'
        );

        $query = "SELECT `table_name` AS 'table_name'
        FROM `information_schema`.`columns`
        WHERE `table_schema` = '" . _DB_NAME_ . "' AND `table_name` LIKE '" . str_replace('_', '\_', _DB_PREFIX_) . "%'
        AND (`column_name` = '" . implode("' OR `column_name` = '", $columns_name) . "')
        " . (!empty($non_tables) ? "AND (`table_name` <> '" . implode("' AND `table_name` <> '", $non_tables) . "')" :
        "") . "
        " . (!empty($tables_name) ? "OR (`table_name` = '" . implode("' OR `table_name` = '", $tables_name) . "')" :
        "") . " GROUP BY `table_name`";

        $listModules = Db::getInstance()->executeS(
            $query
        );

        // Assign Positions
        $positions = array();
        if (Tools::getValue('BCL_GLOBAL_ATT')) {
            $positions['attributes'] = explode(';', Tools::getValue('BCL_GLOBAL_ATT'));
            $positions['tables'] = explode(';', Tools::getValue('BCL_GLOBAL_TAB'));
            $positions['titles'] = explode(';', Tools::getValue('BCL_GLOBAL_TIT'));
            $positions['types'] = explode(';', Tools::getValue('BCL_GLOBAL_TYP'));
        }

        // Assign the default table (table_name)
        $selected_table = Tools::getValue('selecModule') ?
        Tools::getValue('selecModule') : $listModules[0]['table_name'];
        $attributes_available = array();

        if (!Tools::getValue('default') && Tools::getValue('selecModule')) {
            // Assign the available attributes
            $attributes_available[$selected_table] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . pSQL($selected_table) . "`"
            ); // For new tab
        } else {
            $attributes_available[_DB_PREFIX_ . 'product_attribute'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "product_attribute` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'attribute_lang'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "attribute_lang` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'product_attribute_image'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "product_attribute_image` WHERE `Field` = 'id_image'"
            );
            $attributes_available[_DB_PREFIX_ . 'product'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "product` WHERE `Field` NOT LIKE 'id_%'
                AND `Field` NOT LIKE '%quantity%'"
            );
            $attributes_available[_DB_PREFIX_ . 'product_lang'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "product_lang` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'stock_available'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "stock_available` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'category_lang'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "category_lang` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'tag'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "tag` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'manufacturer'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "manufacturer` WHERE `Field` NOT LIKE 'id_%'"
            );
            $attributes_available[_DB_PREFIX_ . 'supplier'] = Db::getInstance()->executeS(
                "SHOW columns FROM `" . _DB_PREFIX_ . "supplier` WHERE `Field` NOT LIKE 'id_%'"
            );
        }

        return $this->context->smarty->assign(array(
            'modules' => $listModules,
            'selected_table' => $selected_table,
            'prefix' => _DB_PREFIX_,
            'refresh' => 'index.php?controller=AdminCombinationsListConfig&add' . $this->table . '&token=' .
              Tools::getAdminTokenLite('AdminCombinationsListConfig') . '&refresh=1',
            'attributes_available' => $attributes_available,
            'positions' => $positions,
            'types' => $this->types,
            'link_to_module' => 'index.php?controller=' . $this->tabClassName . '&token=' .
            Tools::getAdminTokenLite('AdminCombinationsListConfig'),
            'default_list' => $this->getDefaultList()
        ));
    }

    public function renderForm()
    {
        if ($this->assignation()) {
            $admin_form = $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/admin_form/available_tables.tpl');
            $admin_form .= $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/admin_form/available_attributes.tpl');
            $admin_form .= $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/admin_form/position_attributes.tpl');
            $script = $this->context->smarty->fetch($this->module->getLocalPath() .
              'views/templates/admin/admin_form/configure.tpl');

            return $admin_form . $script;
        } elseif (isset($this->warning) || isset($this->success) || isset($this->errors)) {
            $link = new Link();
            $link = $link->getAdminLink('AdminCombinationsListConfig', true);

            Tools::redirectAdmin($link);
        }
    }

    public function renderList()
    {
        if (Tools::isSubmit('submitGlobalForm')) {
            if (Configuration::updateValue('BCL_all_products', (int)Tools::getValue('all_products'), false, 0, 0)) {
                $this->success[] = $this->l("Configuration saved");
            } else {
                $this->errors[] = $this->l("There was an error");
            }
        } elseif (Tools::isSubmit('submitQuickAdd')) {
            $clconfig = new CLConfig();
            if ($clconfig->add()) {
                $this->success[] = $this->l("Successfully added");
                $this->context->smarty->assign(array(
                    'success' => $this->success
                ));
            } else {
                $this->errors[] = $this->l("There was an error");
                $this->context->smarty->assign(array(
                    'errors' => $this->errors
                ));
            }
        }

        if (version_compare(_PS_VERSION_, '1.6.0.12', '<')) {
            $id_table = '#bocombinationslist';
            $id_form = '#bocombinationslist';
        } else {
            $id_table = '#table-bocombinationslist';
            $id_form = '#form-bocombinationslist';
        }

        $this->addRowAction('Delete');
        $this->context->smarty->assign(array(
            'types' => $this->types,
            'quick_add' => $this->quick_add,
            'description' => $this->getDefaultList(),
            'id_table' => $id_table,
            'id_form' => $id_form
        ));
        $quick_add = $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/quick_add.tpl');
        $renderList = $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/renderList.tpl');

        return $quick_add .
            parent::renderList() .
            '<div class="container">' .$this->renderGlobalForm() . '</div>' .
            $renderList;
    }

    protected function renderGlobalForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('General'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'name' => 'all_products',
                        'label' => $this->l('Show all products:'),
                        'desc' => $this->l('With and without combinations.'),
                        'values' => array(
                            array(
                                'id' => 'active',
                                'value' => true,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'inactive',
                                'value' => false,
                                'label' => $this->l('No')
                            )
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Validate'),
                    'id' => 'BCL_submit',
                )
            )
        );

        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
//        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitGlobalForm';
        $helper->currentIndex = self::$currentIndex;
        $helper->token = $this->token;

        $helper->tpl_vars = array(
            'fields_value' => array(
                'all_products' => (bool)Configuration::get('BCL_all_products', null, 0, 0)
            ),
            'languages' => $this->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form));
    }

    public function initProcess()
    {
        if (Tools::isSubmit('submitAddbocombinationslist') || Tools::isSubmit('submitQuickAdd')) {
            $lastPosition = CLConfig::getLastPosition();
            $i = 1;
            $positions = array();
            $attributes = array();
            $tables = array();
            $titles = array();
            $types = array();
        }

        if (Tools::isSubmit('submitAddbocombinationslist')) {
            while (Tools::getValue('BCL_ATTRIBUTE_' . $i)) {
                if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                    $positions[] = $lastPosition + $i;
                } else {
                    $positions[] = $lastPosition + $i - 1;
                }
                $attributes[] = Tools::getValue('BCL_ATTRIBUTE_' . $i);
                $tables[] = Tools::getValue('BCL_TABLE_' . $i);
                $titles[] = Tools::getValue('BCL_TITLE_' . $i);
                $types[] = Tools::getValue('BCL_TYPE_' . $i);
                ++$i;
            }
        } elseif (Tools::isSubmit('submitQuickAdd')) {
            if (version_compare(_PS_VERSION_, '1.6.1.0', '<')) {
                $positions[] = $lastPosition + 1;
            } else {
                $positions[] = $lastPosition;
            }

            $attribute_to_add = explode('-', Tools::getValue('BCL_QUICK_ADD'), 2);
            $default_list = $this->getDefaultList()[Tools::getValue('BCL_QUICK_ADD')];

            $tables[] = $attribute_to_add[0];
            $attributes[] = $attribute_to_add[1];
            $titles[] = isset($default_list['title']) ? $default_list['title'] : "";
            $types[] = $default_list['type'];
        }

        if (Tools::isSubmit('submitAddbocombinationslist') || Tools::isSubmit('submitQuickAdd')) {
            $_POST['positions'] = implode(';', $positions);
            $_POST['attributes'] = implode(';', $attributes);
            $_POST['tables'] = implode(';', $tables);
            $_POST['titles'] = implode(';', $titles);
            $_POST['types'] = implode(';', $types);
        }

        parent::initProcess();
    }

    public function ajaxProcessUpdatePositions()
    {
        $way = (int)Tools::getValue('way');
        $id_combinationslist = (int)Tools::getValue('id');
        $positions = Tools::getValue('bocombinationslist');

        $new_positions = array();
        foreach ($positions as $v) {
            if (count(explode('_', $v)) == 4) {
                $new_positions[] = $v;
            }
        }

        foreach ($new_positions as $position => $value) {
            $pos = explode('_', $value);

            if (isset($pos[2]) && (int)$pos[2] === $id_combinationslist) {
                if ($clconfig = new CLConfig()) {
                    Configuration::updateValue('id_bocombinationslist', $id_combinationslist);
                    if (isset($position) && $clconfig->updatePosition(
                        $way,
                        $position
                    )) {
                        echo 'ok position '.(int)$position.' for id '.(int)$pos[1].'\r\n';
                    } else {
                        echo '{"hasError" : true, "errors" : "Can not update id ' . (int)$id_combinationslist .
                        ' to position ' . (int)$position . ' "}';
                    }
                }

                break;
            }
        }
    }

    public function ajaxProcessSendInputList()
    {
        // AJAX DATAS :
        // CLConfig_input
        // id_bocombinationslist
        // attribute

        Db::getInstance()->execute(
            "UPDATE `" . _DB_PREFIX_ . $this->table . "`
            SET `" . pSQL(Tools::getValue('attribute')) . "` = '" . pSQL(Tools::getValue('BCL_input')) . "'
            WHERE `id_bocombinationslist` = '" . (int)Tools::getValue('id_bocombinationslist') . "'"
        );

        die(true);
    }

    public function postProcess()
    {
        if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            Configuration::updateValue('id_bocombinationslist', (int)Tools::getValue('id_bocombinationslist'));
        }

        parent::postProcess();
    }

    public function ajaxProcessUpdateValue()
    {
        if (Tools::getValue('BCL_value') === false) {
            die(json_encode(array('success' => 0, 'text' => $this->l('Please enter a value.'))));
        }

        $objects = array(
            'product_attribute' => Combination::class,
        );

        $update = $this->ajaxUpdate();

        if (array_key_exists($update['table'], $objects)) {
            $obj = new $objects[$update['table']]($update['id_primary_key']);

            if (!Validate::isLoadedObject($obj)) {
                die(json_encode(array('success' => 0, 'text' => $this->l('An error has occurred.'))));
            }

            switch ($update['table'] . '-' . $update['attribute']) {
                case 'stock_available-quantity':
                    if (Validate::isUnsignedInt(Tools::getValue('BCL_value'))) {
                        StockAvailable::setQuantity((int)$update['id_product'], 0, Tools::getValue('BCL_value'));
                    } else {
                        die(json_encode(array('success' => 0, 'text' => $this->l('An error has occurred.'))));
                    }
                    break;
                case 'product_lang-name':
                    Db::getInstance()->execute(
                        "UPDATE `" . _DB_PREFIX_ . "product_lang` 
                        SET `name` = '" . pSQL(Tools::getValue('BCL_value')) . "'
                        WHERE `id_product` = " . (int)$update['id_product'] . "
                        AND `id_lang` = " . (int)$this->context->language->id
                    );
                    break;
                default:
                    $obj->{$update['attribute']} = pSQL(Tools::getValue('BCL_value'));

                    try {
                        $obj->update();

                        switch ($update['attribute']) {
                            case 'location':
                                StockAvailable::setLocation(
                                    (int)$update['id_product'],
                                    pSQL(Tools::getValue('BCL_value')),
                                    null,
                                    (int)$update['id_product_attribute']
                                );
                                break;
                        }
                    } catch (Exception $e) {
                        die(json_encode(array('success' => 0, 'text' => $e->getMessage())));
                    }
                    break;
            }
        } else {
            $count = Db::getInstance()->getValue(
                "SELECT COUNT(*)
                FROM `" . _DB_PREFIX_ . pSQL($update['table']) . "`
                WHERE 1" . $update['where_update']
            );

            if ($count > 1 || $count <= 0) {
                die(json_encode(array('success' => 0, 'text' => $this->l('You can\'t edit this value.'))));
            } else {
                Db::getInstance()->execute(
                    "UPDATE `" . _DB_PREFIX_ . pSQL($update['table']) . "`
                    SET `" . pSQL($update['attribute']) . "` = '" . pSQL(Tools::getValue('BCL_value')) . "'
                    WHERE 1" . $update['where_update']
                );
            }
        }

        die(json_encode(array('success' => 1, 'text' => $this->l('Successful update.'))));
    }

    public function ajaxUpdate()
    {
        $table = pSQL(Tools::getValue('table'));
        $attribute = PSQL(Tools::getValue('attribute'));
        $id_product_attribute = (int)Tools::getValue('id_product_attribute');
        $bcl = BCL::getInstance();
        $bcl->setWithAliases(false);

//        TODO
//        if (($attribute == 'final_price' || $attribute == 'price_final' || $attribute == 'price') &&
//            ($table == 'product' || $table == 'product_shop')) {
//            $this->ajaxUpdatePrices($id_product, $attribute);
//        }

        $exists = Db::getInstance()->getValue(
            "SELECT `column_name`
            FROM `information_schema`.`COLUMNS`
            WHERE `table_schema` = '" . _DB_NAME_ . "'
            AND `table_name` = '" . _DB_PREFIX_ . pSQL($table) . "'
            AND `column_name` = '" . pSQL($attribute) . "'"
        );

        if (!$exists) {
            die(json_encode(array('success' => 0, 'text' => $this->l('This field does not exist.'))));
        }

        $primary_key = Db::getInstance()->getValue(
            "SELECT `column_name`
            FROM `information_schema`.`COLUMNS`
            WHERE `table_schema` = '" . _DB_NAME_ . "'
            AND `table_name` = '" . _DB_PREFIX_ . pSQL($table) . "'
            AND `column_key` = 'PRI'"
        );

        $where = $bcl->getDefaultWhere();
        $bcl->setDefaultJoin()->addDefaultJoin($table);

        if (!in_array($table, $bcl->default_tables)) {
            // JOIN
            $columns = Db::getInstance()->executeS(
                "SHOW COLUMNS FROM `" . _DB_PREFIX_ . pSQL($table) . "`"
            );
            $return = true;

            $base_on = '';
            $join_on = '';
            $join_table = '';
            $lang = false;

            foreach ($bcl->fields as $join_table_temp => $field) {
                if (is_array($field)) {
                    $join_table_temp = $field['table'];
                } else {
                    $field = array(
                        'base_on' => $field,
                        'join_on' => $field,
                    );
                }

                foreach ($columns as $column) {
                    if ($column['Field'] == $field['join_on'] && $join_on == '') {
                        $base_on = $field['base_on'];
                        $join_on = $field['join_on'];
                        $join_table = $join_table_temp;
                        $bcl->addDefaultJoin($join_table);
                        $return = false;
                    } elseif ($column['Field'] == 'id_lang') {
                        $lang = true;
                    }

                    if ($lang && $join_on != '') {
                        break 2;
                    }
                }
            }

            if ($return) {
                die(Tools::jsonEncode(array('success' => 0, 'text' => $this->l('An error has occurred.'))));
            }
            $on = '`' . pSQL($table) . '`.`' . pSQL($join_on) . '` =
                    `' . pSQL($join_table) . '`.`' . pSQL($base_on) . '`';
            if ($lang && preg_match('/.*_lang$/', $table)) {
                $on .= " AND `" . pSQL($table) . "`.`id_lang` = '" . (int)$this->context->language->id . "'";
            }

            $bcl->joinQuery(
                'left',
                pSQL($join_table),
                pSQL($table),
                pSQL($table),
                $on
            );
        }

        try {
            $row = Db::getInstance()->getRow(
                "SELECT `" . pSQL($table) . "`.*
                FROM `" . _DB_PREFIX_ . "product_attribute` `product_attribute` " .
                implode(' ', $bcl->getJoin()) . " WHERE 1 " . $where .
                (empty($id_product_attribute) ? " " : " AND `product_attribute`.`id_product_attribute` = '" . (int)$id_product_attribute . "' ") .
                "GROUP BY `product_attribute`.`id_product_attribute`
                ORDER BY `product_attribute`.`id_product_attribute` DESC"
            );
        } catch (\Exception $e) {
            die(json_encode(array('success' => 0, 'text' => $e->getMessage())));
        }
        if (empty($row)) {
            die(json_encode(array('success' => 0, 'text' => $this->l('Unable to change value.'))));
        }

        $where_update = "";
        foreach ($row as $key => $column) {
            if ($key != $attribute && !empty($column)) {
                $where_update .= " AND `" . pSQL($key) . "` = '" . pSQL($column, true) . "'";
            }
        }

        return array(
            'table' => $table,
            'attribute' => $attribute,
            'attribute_value' => $row[$attribute],
            'id_primary_key' =>
                isset($row[$primary_key]) && Validate::isInt($row[$primary_key]) ? (int)$row[$primary_key] : 0,
            'where_update' => $where_update,
            'id_product_attribute' => (isset($row['id_product_attribute']) ? (int)$row['id_product_attribute'] : 0),
            'id_product' => (isset($row['id_product']) ? (int)$row['id_product'] : 0),
        );
    }

    public function printTypeSelect($value, $row) {
        $types = $this->types;
        if ($row['table'] !== 'stock_available' && $row['table'] !== 'product_attribute' ||
            substr($row['attribute'], 0, 3) === 'id_') {
            unset($types['editable']);
        }
        $this->context->smarty->assign([
            'selected_value' => $value,
            'types' => $types,
        ]);

        return $this->context->smarty->fetch($this->module->getLocalPath() .
            'views/templates/admin/list/config_type.tpl');
    }

    public function getDefaultList()
    {
        return array(
            //PRODUCT
            'product-id_product' => array(
                'description' => 'ID ' . $this->l('Product'),
                'title' => 'ID ' . $this->l('Product'),
                'type' => false,
            ),
            'product-ean13' => array(
                'description' => 'EAN-13 ' . $this->l('of the product'),
                'title' => 'EAN-13',
                'type' => false,
            ),
            'product-isbn' => array(
                'description' => 'ISBN ' . $this->l('of the product'),
                'title' => 'ISBN',
                'type' => false,
            ),
            'product-upc' => array(
                'description' => 'UPC ' . $this->l('of the product'),
                'title' => 'UPC',
                'type' => false,
            ),
            'product-ecotax' => array(
                'description' => $this->l('Ecotax') . ' ' . $this->l('of the product'),
                'type' => false,
            ),
            'product-minimal_quantity' => array(
                'description' => $this->l('Minimal quantity'),
                'type' => 'integer',
            ),
            'product-low_stock_alert' => array(
                'description' => $this->l('Low stock alert') . ' ' . $this->l('of the product'),
                'type' => false,
            ),
            'product-price' => array(
                'description' => $this->l('Price (Tax Excl.)') . ' ' . $this->l('of the product'),
                'title' => $this->l('Price (Tax Excl.)'),
                'type' => 'price',
            ),
            'product-wholesale_price' => array(
                'description' => $this->l('Wholesale price') . ' ' . $this->l('of the product'),
                'type' => 'price',
            ),
            'product-additional_shipping_cost' => array(
                'description' => $this->l('Additional shipping cost'),
                'type' => 'price',
            ),
            'product-reference' => array(
                'description' => $this->l('Reference') . ' ' . $this->l('of the product'),
                'title' => $this->l('Reference'),
                'type' => false,
            ),
            'product-supplier_reference' => array(
                'description' => $this->l('Supplier reference') . ' ' . $this->l('of the product'),
                'type' => false,
            ),
            'product-location' => array(
                'description' => $this->l('Location') . ' ' . $this->l('of the product'),
                'type' => false,
            ),
            'product-width' => array(
                'description' => $this->l('Width') . ' ' . $this->l('of the product'),
                'title' => $this->l('Width'),
                'type' => false,
            ),
            'product-height' => array(
                'description' => $this->l('Height') . ' ' . $this->l('of the product'),
                'title' => $this->l('Height'),
                'type' => false,
            ),
            'product-depth' => array(
                'description' => $this->l('Depth') . ' ' . $this->l('of the product'),
                'title' => $this->l('Depth'),
                'type' => false,
            ),
            'product-weight' => array(
                'description' => $this->l('Weight') . ' ' . $this->l('of the product'),
                'title' => $this->l('Weight'),
                'type' => 'decimal',
            ),
            'product-additional_delivery_times' => array(
                'description' => $this->l('Is there additional delivery times?'),
                'type' => 'boolean',
            ),
            'product-quantity_discount' => array(
                'description' => $this->l('Is there a quantity discount?'),
                'type' => 'boolean',
            ),
            'product-active' => array(
                'description' => $this->l('Is it active? (State)'),
                'title' => $this->l('Status'),
                'type' => 'boolean',
            ),
            'product-available_for_order' => array(
                'description' => $this->l('Is it available for order?'),
                'type' => 'boolean',
            ),
            'product-available date' => array(
                'description' => $this->l('Available date'),
                'type' => 'date',
            ),
            'product-date_add' => array(
                'description' => $this->l('Date when the product was created'),
                'type' => 'date',
            ),
            'product-date_upd' => array(
                'description' => $this->l('Date when the product was updated'),
                'type' => 'date',
            ),

            //PRODUCT_LANG
            'product_lang-description' => array(
                'description' => $this->l('Description of the product'),
                'type' => false,
            ),
            'product_lang-description_short' => array(
                'description' => $this->l('Short description of the product'),
                'type' => false,
            ),
            'product_lang-name' => array(
                'description' => $this->l('Name of the product'),
                'title' => $this->l('Name'),
                'type' => false,
            ),

            //CATEGORY_LANG
            'category_lang-name' => array(
                'description' => $this->l('Name of the category'),
                'title' => $this->l('Category'),
                'type' => false,
            ),
            'description-name' => array(
                'description' => $this->l('Description of the category'),
                'type' => false,
            ),

            //IMAGE
            'product_attribute_image-id_image' => array(
                'description' => $this->l('Image of the product'),
                'title' => $this->l('Image'),
                'type' => false,
            ),

            //STOCK_AVAILABLE
            'stock_available-quantity' => array(
                'description' => $this->l('Quantity of the product'),
                'title' => $this->l('Quantity'),
                'type' => 'integer',
            ),
            'stock_available-physical_quantity' => array(
                'description' => $this->l('Physical quantity of the product'),
                'type' => 'integer',
            ),
            'stock_available-reserved_quantity' => array(
                'description' => $this->l('Reserved quantity (sells include)'),
                'type' => 'integer',
            ),
            'stock_available-location' => array(
                'description' => $this->l('Stock location'),
                'type' => false,
            ),

            //TAG
            'tag-name' => array(
                'description' => $this->l('Tags of the product'),
                'title' => $this->l('Tags'),
                'type' => false,
            ),

            //MANUFACTURER
            'manufacturer-name' => array(
                'description' => $this->l('The name of the brand'),
                'title' => $this->l('Brand'),
                'type' => false,
            ),

            //SUPPLIER
            'supplier-name' => array(
                'description' => $this->l('The name of the supplier'),
                'title' => $this->l('Supplier'),
                'type' => false,
            ),

            //ATTRIBUTE_LANG
            'attribute_lang-name' => array(
                'description' => $this->l('Attributes name'),
                'title' => $this->l('Name'),
                'type' => false,
            ),

            //PRODUCT_ATTRIBUTE
            'product_attribute-id_product_attribute' => array(
                'description' => 'ID ' . $this->l('Combination'),
                'title' => 'ID ' . $this->l('Combination'),
                'type' => 'integer',
            ),
            'product_attribute-ean13' => array(
                'description' => 'EAN-13',
                'title' => 'EAN-13',
                'type' => false,
            ),
            'product_attribute-isbn' => array(
                'description' => 'ISBN',
                'title' => 'ISBN',
                'type' => false,
            ),
            'product_attribute-upc' => array(
                'description' => 'UPC',
                'title' => 'UPC',
                'type' => false,
            ),
            'product_attribute-ecotax' => array(
                'description' => $this->l('Ecotax'),
                'type' => false,
            ),
            'product_attribute-minimal_quantity' => array(
                'description' => $this->l('Minimal quantity'),
                'type' => 'integer',
            ),
            'product_attribute-low_stock_alert' => array(
                'description' => $this->l('Low stock alert'),
                'type' => false,
            ),
            'product_attribute-price' => array(
                'description' => $this->l('Price (Tax Excl.)'),
                'title' => $this->l('Price (Tax Excl.)'),
                'type' => 'price',
            ),
            'product_attribute-wholesale_price' => array(
                'description' => $this->l('Wholesale price'),
                'type' => 'price',
            ),
            'product_attribute-reference' => array(
                'description' => $this->l('Reference'),
                'title' => $this->l('Reference'),
                'type' => false,
            ),
            'product_attribute-supplier_reference' => array(
                'description' => $this->l('Supplier reference'),
                'title' => $this->l('Supplier reference'),
                'type' => false,
            ),
            'product_attribute-location' => array(
                'description' => $this->l('Location'),
                'title' => $this->l('Location'),
                'type' => false,
            ),
            'product_attribute-weight' => array(
                'description' => $this->l('Weight'),
                'title' => $this->l('Weight'),
                'type' => 'decimal',
            ),

            //FEATURE_VALUE_LANG
            'feature_value_lang-value' => array(
                'description' => $this->l('Features'),
                'title' => $this->l('Features'),
                'type' => false,
            ),

            //BOCOMBINATIONSLIST
            'bocombinationslist-price' => array(
                'description' =>
                    $this->l('Price (Tax Excl.)') . ' (' . $this->l('product') . ' + ' . $this->l('combination') . ')',
                'title' => $this->l('Price (Tax Excl.)'),
                'type' => 'price',
            ),
            'bocombinationslist-final_price' => array(
                'description' =>
                    $this->l('Price (Tax Incl.)') . ' (' . $this->l('product') . ' + ' . $this->l('combination') . ')',
                'title' => $this->l('Price (Tax Incl.)'),
                'type' => 'price',
            ),
            'bocombinationslist-weight' => array(
                'description' =>
                    $this->l('Weight') . ' (' . $this->l('product') . ' + ' . $this->l('combination') . ')',
                'title' => $this->l('Weight'),
                'type' => 'decimal',
            ),
        );
    }
}
