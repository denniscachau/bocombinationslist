<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_bddba607a67aaea03156a1c5628b85df'] = 'Lista de combinaciones';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_2c3a50704cbae05705304332cbfd8e2f'] = 'Mostrar una lista de todas las combinaciones con columnas personalizadas';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_bb8956c67b82c7444a80c6b2433dd8b4'] = '¿Está seguro de que desea desinstalar este módulo?';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_deb10517653c255364175796ace3553f'] = 'Producto';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_47ac923d219501859fb68fed8c8db77b'] = 'Combinación';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_be53a0541a6d36f6ecb879fa2c584b08'] = 'Imagen';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_287234a1ff35a314b5b6bc4e5828e745'] = 'Atributos';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_d802cdd26108941c2ceb197be9caa30b'] = 'Precio (sin IVA)';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_bf794ad2cabe66da3ef83fb978dd4ba1'] = 'Precio (con IVA)';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_694e8d1f2ee056f98ee488bdc4982d73'] = 'Cantidad';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_b9208b03bcc9eb4a336258dcdcb66207'] = 'Combinaciones';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_691d1860ec58dd973e803e209697d065'] = 'Liza';
$_MODULE['<{bocombinationslist}prestashop>bocombinationslist_6d66f32ddd41305a4a5a9ff9f28fdb5d'] = 'Personalizar las columnas de la lista de combinaciones';
$_MODULE['<{bocombinationslist}prestashop>enablelist_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activado';
$_MODULE['<{bocombinationslist}prestashop>enablelist_b9f5c797ebbf55adccdd8539a65a0241'] = 'Discapacitado';
$_MODULE['<{bocombinationslist}prestashop>renderlist_d4eab6753f9f3d4b57580427264beaa6'] = 'Actualización exitosa';
$_MODULE['<{bocombinationslist}prestashop>renderlist_8db5bd2272b51b3525ad7661c10d8b2b'] = 'Se ha producido un error';
$_MODULE['<{bocombinationslist}prestashop>global_form_0db377921f4ce762c62526131097968f'] = 'General';
$_MODULE['<{bocombinationslist}prestashop>global_form_aa7b55b66b1e46180880bd241e65481e'] = 'Mostrar todos los productos:';
$_MODULE['<{bocombinationslist}prestashop>global_form_93cba07454f06a4a960172bbd6e2a435'] = 'Sí';
$_MODULE['<{bocombinationslist}prestashop>global_form_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{bocombinationslist}prestashop>global_form_68b5809c953bfeb2bdb1a8b930743e3d'] = 'Con y sin combinaciones';
$_MODULE['<{bocombinationslist}prestashop>global_form_876ac16f671c12a4cf8a44e4b7034fb7'] = 'Adición rápida';
$_MODULE['<{bocombinationslist}prestashop>global_form_ec211f7c20af43e742bf2570c3cb84f9'] = 'Agregar';
$_MODULE['<{bocombinationslist}prestashop>global_form_2f75d5965fcb5ae9ebda0ccb001ebd58'] = 'Si no encuentra el atributo que desea agregar, haga clic en el botón \"Agregar una nueva columna\" en la parte superior derecha de la página.';
$_MODULE['<{bocombinationslist}prestashop>global_form_ad3d06d03d94223fa652babc913de686'] = 'Validar';
$_MODULE['<{bocombinationslist}prestashop>available_attributes_b7400332dfe3450fdbffdb212f090920'] = 'Atributos disponibles';
$_MODULE['<{bocombinationslist}prestashop>available_attributes_51c45b795d5d18a3e4e0c37e8b20a141'] = 'Tabla';
$_MODULE['<{bocombinationslist}prestashop>available_attributes_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Descripción';
$_MODULE['<{bocombinationslist}prestashop>available_attributes_ec211f7c20af43e742bf2570c3cb84f9'] = 'Agregar';
$_MODULE['<{bocombinationslist}prestashop>position_attributes_39204db149e8b3b2ec4f12624de909c4'] = 'Posición de los atributos';
$_MODULE['<{bocombinationslist}prestashop>position_attributes_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{bocombinationslist}prestashop>position_attributes_ad3d06d03d94223fa652babc913de686'] = 'Validar';
$_MODULE['<{bocombinationslist}prestashop>position_attributes_ea4788705e6873b424c65e91c2846b19'] = 'Cancelar';
$_MODULE['<{bocombinationslist}prestashop>configure_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{bocombinationslist}prestashop>configure_8dbbe5098b6dfad060d8fc57a0ebe2c6'] = 'Adición exitosa';
$_MODULE['<{bocombinationslist}prestashop>available_tables_111118f909f12d8bb8b39ea920edfeb0'] = 'Elección de la tabla';
$_MODULE['<{bocombinationslist}prestashop>available_tables_4f490697761d839d99bd06bb0224d627'] = 'Tablas disponibles:';
$_MODULE['<{bocombinationslist}prestashop>available_tables_cabfbd11f7acaf2901efe20a871bdd19'] = 'Cambiar la tabla (sin guía)';
$_MODULE['<{bocombinationslist}prestashop>available_tables_1eb2cf386f775968fa109ec2ae3269d2'] = 'Cambiar a tabla por defecto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_52f5e0bc3859bc5f5e25130b6c7e8881'] = 'Posición';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_287234a1ff35a314b5b6bc4e5828e745'] = 'Atributos';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_51c45b795d5d18a3e4e0c37e8b20a141'] = 'Tabla';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_b78a3223503896721cca1303f776159b'] = 'Título';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_8da124923942e61b3a9add617f643d32'] = 'Booleano (verdadero / falso)';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_3601146c4e948c32b6424d2c0a7f0118'] = 'Precio';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_a0faef0851b4294c06f2b94bb1cb2044'] = 'Entero';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_937504f318f04eaf0f1d701df4c4d7f3'] = 'Decimal';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_37be07209f53a5d636d5c904ca9ae64c'] = 'Porcentaje';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_44749712dbec183e983dcd78a7736c41'] = 'Fecha';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_34bb8fbfb47885fe47470874152c319d'] = 'Fecha y hora';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_2c09e798bf8c5a82d211331c82893a0f'] = 'Editable';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_7a1920d61156abc05a60135aefe8bc67'] = 'Defecto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_47ac923d219501859fb68fed8c8db77b'] = 'Combinación';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_deb10517653c255364175796ace3553f'] = 'Producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_0557fa923dcee4d0f86b1409f5c2167f'] = 'Atrás';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_45a46eabac93dde89871f9e1d57817df'] = 'Agregar una nueva columna';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_4d1c8263ba1036754f8db14a98f9f006'] = 'Recargar';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_7feff43ab21f15d13ba477e2a14ce9f5'] = 'del producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_e92cfa244b5eb9025d07522080468445'] = 'Ecotasa';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_ac0b7d0703609cbbfa09b53f8d793d29'] = 'Cantidad mínima';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_24eb41ae265b268c0ce14e1fef872399'] = 'Alerta de stock bajo';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_d802cdd26108941c2ceb197be9caa30b'] = 'Precio (sin IVA)';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_c804723ccdde3d7a46933b208c6f928d'] = 'Precio de compra';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_7bbdd372c9d171174e3ce1ba9d97d5f6'] = 'Costo de envío adicional';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_63d5049791d9d79d86e9a108b0a999ca'] = 'Referencia';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_8284ae5df53e6e7ffc1f2cc67ae68765'] = 'Referencia del proveedor';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_ce5bf551379459c1c61d2a204061c455'] = 'Localización';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_32954654ac8fe66a1d09be19001de2d4'] = 'Ancho';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_eec6c4bdbd339edf8cbea68becb85244'] = 'Altura';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_675056ad1441b6375b2c5abd48c27ef1'] = 'Profundidad';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_8c489d0946f66d17d73f26366a4bf620'] = 'Peso';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_22232b1149de63de3a68d832406321f8'] = '¿Hay tiempos de entrega adicionales?';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_c562c3dd26e5717d7262450f5247275b'] = '¿Existe un descuento por cantidad?';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_be8e80d576231cbe24b000cbd110e19e'] = '¿Está activo? (Estado)';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_ec53a8c4f07baed5d8825072c89799be'] = 'Estado';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_ca9c8c69f7ef9c87b4887017b920bd30'] = '¿Está disponible para ordenar?';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_de8304421c13baaf6b13b9ed84d73eb7'] = 'Fecha disponible';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_fe5038bee4d1853f943468d395727df7'] = 'Fecha de creación del producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_93a1aebc58a9fe94c9590133f95c393c'] = 'Fecha en la que se actualizó el producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_09c2e1026952b086c78323051001a0e8'] = 'Descripción del producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_c6245a75a10852595815161cbd4dc431'] = 'Breve descripción del producto.';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_68144310d2ea225be0cddaea8345d821'] = 'Nombre del producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_49ee3087348e8d44e1feda1917443987'] = 'Nombre';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_07232cd2a18a174f8e67ccdbc512edad'] = 'Nombre de la categoría';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categoría';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_4ecbdf73d2263fd64c7dfcccbbb91fde'] = 'Descripción de la categoría';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_b72dc4f8c901c90347912fbee41326e8'] = 'Imagen del producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_be53a0541a6d36f6ecb879fa2c584b08'] = 'Imagen';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_bd958e2f216b79c9deb4e95ab17cd439'] = 'Cantidad de producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_694e8d1f2ee056f98ee488bdc4982d73'] = 'Cantidad';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_1c2c40f4198c0e66a64f77c607ad10f1'] = 'Cantidad física del producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_e169517aef8d635b269420910921c8a8'] = 'Cantidad reservada (incluye ventas)';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_cdba184783a0253218efd734da81a9da'] = 'Ubicación de stock';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_bc37cebb67728bbb50f9c55c1fe727cd'] = 'Etiquetas del producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_189f63f277cd73395561651753563065'] = 'Etiquetas';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_2c8e338e4d982c9840052977a4563b51'] = 'El nombre de la marca';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_1be6f9eb563f3bf85c78b4219bf09de9'] = 'Marca';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_af88c897ad4d1ff2ae3d0f29bc7b5cc9'] = 'El nombre del proveedor';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_ec136b444eede3bc85639fac0dd06229'] = 'Proveedor';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_89ec50328328395275a746cf146a9e05'] = 'Nombre de los atributos';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_98f770b0af18ca763421bac22b4b6805'] = 'Características';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_f5bf48aa40cad7891eb709fcf1fde128'] = 'producto';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_b968fe9114262b354f14abc84b64fc0b'] = 'combinación';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslistconfig_bf794ad2cabe66da3ef83fb978dd4ba1'] = 'Precio (IVA incluido)';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslist_93cba07454f06a4a960172bbd6e2a435'] = 'Sí';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslist_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslist_b4a34d3f58b039e7685c2e39b5413757'] = 'Actualización exitosa.';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslist_c69732cc923305ac0684ac8fc05a4bca'] = 'Se ha producido un error.';
$_MODULE['<{bocombinationslist}prestashop>admincombinationslist_99d708ed32fbfbdb7555fc4bfe7120b8'] = 'No se puede cambiar el valor.';
