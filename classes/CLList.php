<?php
/**
* 2007-2023 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2023 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CLList
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var string
     */
    private $configuration_table = 'bocombinationslist';

    /**
     * @var string[]
     */
    public $default_tables;

    /**
     * @var string[]
     */
    private $aliases;

    /**
     * @var string[]
     */
    public $fields;

    public function __construct()
    {
        $this->context = Context::getContext();
    }

    public function recoverData()
    {
        $configurations = $this->getConfigurations();

        $query = array();

        $query['where'] = $this->getDefaultWhere();
        $query['join'] = $this->getDefaultJoin();
        $query['group'] = $this->getDefaultGroupBy();
        $query['orderBy'] = $this->getDefaultOrderBy();
        $query['orderWay'] = $this->getDefaultOrderWay();

        $query['select'] = "`a`.`id_product`";
        $query['select'] .= ", `product_attribute`.`id_product_attribute`";

        $config_tables = $configurations['config_tables'];
        $tables = array_unique($config_tables);
        $ndefault_tables = array();

        $select_fields = $this->getSelectFields();

        $attributes_as_list = array(
            'product_attribute_imageid_image',
            'iid_image'
        );

        // DEFAULT TABLES
        foreach ($tables as $table) {
            foreach ($this->default_tables as $table_default) {
                switch ($table) {
                    case $table_default:
                        break;
                    case 'tag':
                        $query['join'] .=  " LEFT JOIN `" . _DB_PREFIX_ . "product_tag` `product_tag`
                        ON `a`.`id_product` = `product_tag`.`id_product`";

                        if (version_compare(_PS_VERSION_, '1.6.1.0', '>=')) {
                            $query['join'] .= " AND `product_tag`.`id_lang` =
                            '" . (int)$this->context->language->id . "'";
                        }
                        $query['join'] .=  " LEFT JOIN `" . _DB_PREFIX_ . "tag` `tag`
                        ON `product_tag`.`id_tag` = `tag`.`id_tag`
                        AND `tag`.`id_lang` = '" . (int)$this->context->language->id . "'";
                        break;
                    case 'attribute_lang':
                        $query['join'] .= " LEFT JOIN `" . _DB_PREFIX_ . "product_attribute_combination`
                        `product_attribute_combination` ON `product_attribute`.`id_product_attribute` =
                        `product_attribute_combination`.`id_product_attribute`";
                        $query['join'] .= " LEFT JOIN `" . _DB_PREFIX_ . "attribute_lang` `attribute_lang`
                        ON `product_attribute_combination`.`id_attribute` = `attribute_lang`.`id_attribute`
                        AND `attribute_lang`.`id_lang` = '" . (int)$this->context->language->id . "'";
                        $query['join'] .= " LEFT JOIN `" . _DB_PREFIX_ . "attribute` `attribute`
                        ON `product_attribute_combination`.`id_attribute` = `attribute`.`id_attribute`";
                        break;

                    default:
                        continue 2;
                }
                continue 2;
            }
            $ndefault_tables[] = $table;
        }

        // JOIN
        if (!empty($ndefault_tables)) {
            foreach ($ndefault_tables as $table) {
                if (empty($table)) {
                    continue;
                }
                $columns = Db::getInstance()->executeS(
                    "SHOW COLUMNS FROM `" . _DB_PREFIX_ . pSQL($table) . "`"
                );
                $join_attribute = '';
                $join_table = '';
                $lang = false;

                foreach ($this->fields as $join_table_temp => $field) {
                    foreach ($columns as $column) {
                        if ($column['Field'] == $field && $join_attribute == '') {
                            $join_attribute = $field;
                            $join_table = $join_table_temp;
                        } elseif ($column['Field'] == 'id_lang') {
                            $lang = true;
                        }

                        if ($lang && $join_attribute != '') {
                            break 2;
                        }
                    }
                }

                $query['join'] .=  " LEFT JOIN `" . _DB_PREFIX_ . pSQL($table) . "` `" . pSQL($table) . "`
                  ON `" . pSQL($table) . "`.`" . pSQL($join_attribute) . "` =
                  `" . pSQL($join_table) . "`.`" . pSQL($join_attribute) . "`";
                if ($lang && preg_match('/.*_lang$/', $table)) {
                    $query['join'] .=
                      " AND `" . pSQL($table) . "`.`id_lang` = '" . (int)$this->context->language->id . "'";
                }
            }
        }

        $array_tables = $configurations['array_tables'];
        $array_attributes = $configurations['array_attributes'];
        $array_titles = $configurations['array_titles'];
        $array_types = $configurations['array_types'];

        $count_booleans = 0;

        foreach ($array_attributes as $key => $attribute) {
            if (empty($attribute)) {
                continue;
            }

            $search = true;
            $attribute_as = pSQL($array_tables[$key] . $attribute);
            if (in_array($attribute_as, $attributes_as_list)) {
                $attribute_as = $attribute;
            }

            switch ($attribute_as) {
                case 'id_image':
                    $query['select'] .= ", IF(
                            `product_attribute_image`.`id_image` = 0,
                            `i`.`id_image`,
                            `product_attribute_image`.`id_image`
                        ) AS '" . pSQL($attribute_as) . "'";
                    $search = false;
                    break;

                case 'combinationslist_price':
                case 'combinationslist_weight':
                    $query['select'] .=
                        ", (`product_attribute`.`" . pSQL($attribute) . "` + `a`.`" . pSQL($attribute) . "`)
                        AS '" . pSQL($attribute_as) . "'";
                    break;

                default:
                    if ($config_tables[$key] == 'combinationslist') {
                        $query['select'] .= ", 0 AS '" . pSQL($attribute_as) . "'";
                        $search = false;
                    } else {
                        $query['select'] .=
                            ", GROUP_CONCAT(DISTINCT`" . pSQL($array_tables[$key]) . "`.`" . pSQL($attribute) . "`)
                                AS '" . pSQL($attribute_as) . "'";
                    }
                    break;
            }

            if (!empty($array_titles[$key])) {
                $query['fields_list'][$attribute_as] = array(
                  'title' => $array_titles[$key],
                  'align' => 'text-center',
                );
            } else {
                $query['fields_list'][$attribute_as] = array(
                  'title' => $attribute,
                  'align' => 'text-center',
                );
            }

            switch ($array_types[$key]) {
                case 'boolean':
                    $data_type = Db::getInstance()->getValue(
                        "SELECT `data_type`
                        FROM `information_schema`.`COLUMNS`
                        WHERE `table_schema` = '" . _DB_NAME_ . "'
                        AND `table_name` = '" . _DB_PREFIX_ . pSQL($config_tables[$key]) . "'
                        AND `column_name` = '" . pSQL($attribute) . "'"
                    );

                    if ($data_type == "tinyint") {
                        $query['fields_list'][$attribute_as]['type'] = 'bool';
                        if ($count_booleans <= 7) {
                            $query['fields_list'][$attribute_as]['callback'] = 'printCustomBoolean' . $count_booleans;
                        }
                    }
                    $count_booleans++;
                    break;
                case 'price':
                    $query['fields_list'][$attribute_as]['type'] = 'price';
                    break;
                case 'percent':
                    $query['fields_list'][$attribute_as]['type'] = 'percent';
                    break;
                case 'date':
                    $query['fields_list'][$attribute_as]['type'] = 'date';
                    break;
                case 'datetime':
                    $query['fields_list'][$attribute_as]['type'] = 'datetime';
                    break;
            }

            // id = id_feature, table = feature_lang, filter_key = feature_product

            if (in_array($array_tables[$key], array_keys($select_fields))) {
                $select_values = Db::getInstance()->executeS(
                    "SELECT `" . pSQL($select_fields[$array_tables[$key]]['id']) . "` AS 'id',
                    `" . pSQL($select_fields[$array_tables[$key]]['name']) . "` AS `name`
                    FROM `" . _DB_PREFIX_ . pSQL($select_fields[$array_tables[$key]]['table']) . "`" .
                    ($select_fields[$array_tables[$key]]['lang'] ?
                    "WHERE `id_lang` = '" . (int)$this->context->language->id . "'" :
                    "") . " ORDER BY `name` ASC"
                );
                $select_display = array();

                foreach ($select_values as $select_value) {
                    $select_display[$select_value['id']] = $select_value['name'];
                }

                $query['fields_list'][$attribute_as]['type'] = 'select';
                $query['fields_list'][$attribute_as]['list'] = $select_display;
                $query['fields_list'][$attribute_as]['filter_key'] =
                (isset($select_fields[$array_tables[$key]]['filter_key']) ?
                $select_fields[$array_tables[$key]]['filter_key'] :
                $array_tables[$key]) . '!' . $select_fields[$array_tables[$key]]['id'];
                continue;
            }

            if ($attribute_as == "id_image") {
                $query['fields_list'][$attribute_as]['image'] = 'p';
                $query['fields_list'][$attribute_as]['image_id'] = 'id_image';
            } elseif (!$search && $config_tables[$key] != 'combinationslist') {
                $query['fields_list'][$attribute_as]['filter_key'] = $array_tables[$key] . '!' . $attribute;
            }
            $query['fields_list'][$attribute_as]['havingFilter'] = true;
            $query['fields_list'][$attribute_as]['search'] = $search;

            if (preg_match('/^id_\w+/', $attribute)) {
                $query['fields_list'][$attribute_as]['class'] = 'fixed-width-xs';
            }
        }

        return $query;
    }

    public function getDefaultJoin()
    {
        $join = " JOIN `" . _DB_PREFIX_ . "product_shop` " . $this->aliases['product_shop'] . "
            ON `" . $this->aliases['product'] . "`.`id_product` =
            `" . $this->aliases['product_shop'] . "`.`id_product`";

        $join .= " LEFT JOIN `" . _DB_PREFIX_ . "category_lang` " . $this->aliases['category_lang'] . "
            ON `" . $this->aliases['product_shop'] . "`.`id_category_default` =
            `" . $this->aliases['category_lang'] . "`.`id_category`
            AND `" . $this->aliases['category_lang'] . "`.`id_lang` = '" . (int)$this->context->language->id . "'";

        $join .= " LEFT JOIN `" . _DB_PREFIX_ . "category` " . $this->aliases['category'] . "
            ON `" . $this->aliases['category'] . "`.`id_category` =
            `" . $this->aliases['category_lang'] . "`.`id_category`";

        $join .= " LEFT JOIN (
            SELECT `a`.`id_product`, `b`.`reference`, `b`.`supplier_reference`, `b`.`location`, `b`.`ean13`, " .
            (version_compare(_PS_VERSION_, '1.7.0.0', '>=') ? '`b`.`isbn`, ' : '') .
            (version_compare(_PS_VERSION_, '1.7.3.0', '>') ?
                '`b`.`low_stock_threshold`, `b`.`low_stock_alert`, ' : '') .
            " `b`.`upc`,`b`.`wholesale_price`, `b`.`price`, `b`.`ecotax`, `b`.`quantity`,
            `b`.`weight`, `b`.`default_on`, `b`.`minimal_quantity`, `b`.`available_date`,
            IF(`b`.`unit_price_impact` IS NULL, 0, `b`.`unit_price_impact`) AS 'unit_price_impact',
            IF(`b`.`id_product_attribute` IS NULL, 0, `b`.`id_product_attribute`) AS 'id_product_attribute'
            FROM `" . _DB_PREFIX_ . "product` `a` LEFT JOIN `" . _DB_PREFIX_ . "product_attribute` `b`
            ON `a`.`id_product` = `b`.`id_product`)  `product_attribute`
            ON `" . $this->aliases['product'] . "`.`id_product` = `product_attribute`.`id_product`";

        $join .= " LEFT JOIN `" . _DB_PREFIX_ . "product_attribute_shop` `product_attribute_shop`
           ON `product_attribute`.`id_product_attribute` = `product_attribute_shop`.`id_product_attribute`
           AND `product_attribute`.`id_product` = `product_attribute_shop`.`id_product`";

        // $join .= Shop::addSqlAssociation('product_attribute', 'product_attribute_shop');

        $join .= " LEFT JOIN `" . _DB_PREFIX_ . "product_attribute_image` `product_attribute_image`
            ON `product_attribute`.`id_product_attribute` = `product_attribute_image`.`id_product_attribute`";

        // if (version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
        //     $join .= " LEFT JOIN " . _DB_PREFIX_ . "image " . $this->aliases['image'] . "
        //         ON `" . $this->aliases['product'] . "`.`id_product` = `" . $this->aliases['image'] . "`.`id_product`
        //     AND `" . $this->aliases['image'] . "`.`cover` = '1'";
        //
        //     $join .= " LEFT JOIN `" . _DB_PREFIX_ . "image_shop` `" . $this->aliases['image_shop'] . "`
        //         ON " . $this->aliases['image'] . ".`id_image` = `" . $this->aliases['image_shop'] . "`.`id_image`";
        // } else {
            $join .= " LEFT JOIN `" . _DB_PREFIX_ . "image_shop` `" . $this->aliases['image_shop'] . "`
            ON `" . $this->aliases['product'] . "`.`id_product` = `" . $this->aliases['image_shop'] . "`.`id_product`
            AND `" . $this->aliases['image_shop'] . "`.`cover` = '1'";

            $join .= " LEFT JOIN `" . _DB_PREFIX_ . "image` `" . $this->aliases['image'] . "`
                ON `" . $this->aliases['image'] . "`.`id_image` = `" . $this->aliases['image_shop'] . "`.`id_image`";
        // }

        $join .= " LEFT JOIN `" . _DB_PREFIX_ . "stock_available` `stock_available`
            ON `stock_available`.`id_product` = `" . $this->aliases['product'] . "`.`id_product`
            AND `product_attribute`.`id_product_attribute` = `stock_available`.`id_product_attribute`" .
            StockAvailable::addSqlShopRestriction(null, null, 'stock_available');

        $join .= " LEFT JOIN `" . _DB_PREFIX_ . "manufacturer` `manufacturer`
            ON `" . $this->aliases['product'] . "`.`id_manufacturer` = `manufacturer`.`id_manufacturer`";

        $join .= " LEFT JOIN `" . _DB_PREFIX_ . "supplier` `supplier`
            ON `" . $this->aliases['product'] . "`.`id_supplier` = `supplier`.`id_supplier`";

        $join .= " LEFT JOIN `" . _DB_PREFIX_ . "feature_product` `feature_product`
            ON `" . $this->aliases['product'] . "`.`id_product` = `feature_product`.`id_product`";

        return $join;
    }

    public function getDefaultWhere()
    {
        $where = Shop::addSqlRestriction(false, $this->aliases['product_shop']);
//        $where .= Shop::addSqlRestriction(false, 'product_attribute_shop');
        $where .= Shop::addSqlRestriction(false, $this->aliases['product_lang']);
        $where .= Shop::addSqlRestriction(false, $this->aliases['category_lang']);
//        $where .= Shop::addSqlRestriction(false, 'stock_available');

        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $where .= " AND `" . $this->aliases['product'] . "`.`state` = '1'";
        }

        if (!(bool)Configuration::get('BCL_all_products')) {
            $where .= " AND `product_attribute`.`id_product_attribute` != '0'";
        }

        return $where;
    }

    public function getDefaultGroupBy()
    {
        return "GROUP BY `product_attribute`.`id_product_attribute`,
            `" . $this->aliases['product'] . "`.`id_product`";
    }

    public function getDefaultOrderBy()
    {
        return "id_product";
    }

    public function getDefaultOrderWay()
    {
        return "DESC";
    }

    public function getSelectFields()
    {
        return array(
            'manufacturer' => array(
                'id' => 'id_manufacturer',
                'table' => 'manufacturer',
                'name' => 'name',
                'lang' => false
            ),
            'supplier' => array(
                'id' => 'id_supplier',
                'table' => 'supplier',
                'name' => 'name',
                'lang' => false
            ),
            'cl' => array(
                'id' => 'id_category',
                'table' => 'category_lang',
                'name' => 'name',
                'lang' => true
            ),
            'attribute_lang' => array(
                'id' => 'id_attribute_group',
                'table' => 'attribute_group_lang',
                'name' => 'name',
                'filter_key' => 'attribute',
                'lang' => true
            ),
            'feature_value_lang' => array(
                'id' => 'id_feature',
                'table' => 'feature_lang',
                'name' => 'name',
                'filter_key' => 'feature_product',
                'lang' => true
            ),
        );
    }

    public function getParameters($with_aliases = true)
    {
        $this->aliases = array(
            'product' => ($with_aliases ? 'a' :'product'),
            'product_lang' => ($with_aliases ? 'b' :'product_lang'),
            'category' => ($with_aliases ? 'c' :'category'),
            'category_lang' => ($with_aliases ? 'cl' :'category_lang'),
            'image' => ($with_aliases ? 'i' :'image'),
            'image_shop' => ($with_aliases ? 'is' :'image_shop'),
            'product_shop' => ($with_aliases ? 'sa' :'product_shop'),
            'combinationslist' => ($with_aliases ? 'combinationslist_' :'combinationslist')
        );

        $this->fields = array(
            'product_attribute' => 'id_product_attribute',
            $this->aliases['product'] => 'id_product',
            $this->aliases['category'] => 'id_category',
            'manufacturer' => 'id_manufacturer',
            'supplier' => 'id_supplier',
            'feature_product' => 'id_feature_value'
        );

        $this->default_tables = array(
            'product',
            'product_lang',
            'category',
            'category_lang',
            'product_attribute_image',
            'image',
            'image_shop',
            'product_attribute',
            'stock_available',
            'product_shop',
            'manufacturer',
            'supplier',
            'feature_product',
            'combinationslist'
        );
    }

    public function getConfigurations($with_aliases = true)
    {
        $return = array();

        $return['configurations'] = Db::getInstance()->getRow(
            "SELECT
            GROUP_CONCAT(`table` ORDER BY `position` ASC SEPARATOR ';') as 'tables',
            GROUP_CONCAT(`attribute` ORDER BY `position` ASC SEPARATOR ';') as 'attributes',
            GROUP_CONCAT(`title` ORDER BY `position` ASC SEPARATOR ';') as 'titles',
            GROUP_CONCAT(`type` ORDER BY `position` ASC SEPARATOR ';') as 'types'
            FROM `" . _DB_PREFIX_ . $this->configuration_table . "`"
        );

        $this->getParameters($with_aliases);

        $return['array_tables'] = explode(';', $return['configurations']['tables']);

        foreach ($this->default_tables as $table_to_min) {
            if (!array_key_exists($table_to_min, $this->aliases)) {
                continue;
            }
            $return['array_tables'] = preg_replace(
                '/^' . $table_to_min . '$/',
                $this->aliases[$table_to_min],
                $return['array_tables']
            );
        }

        // Initialisation
        $return['array_attributes'] = explode(';', $return['configurations']['attributes']);
        $return['config_tables'] = explode(';', $return['configurations']['tables']);
        $return['array_titles'] = explode(';', $return['configurations']['titles']);
        $return['array_types'] = explode(';', $return['configurations']['types']);

        return $return;
    }

    public static function getColumns($only_booleans = false)
    {
        if ($only_booleans) {
            $where = "type = 'boolean'";
        }

        return Db::getInstance()->executeS(
            "SELECT `table`, `attribute`, `title`, `type`
            FROM `" . _DB_PREFIX_ . "bocombinationslist`
            WHERE 1 AND " . ($only_booleans ? $where : '1') . "
            ORDER BY `position`"
        );
    }
}
